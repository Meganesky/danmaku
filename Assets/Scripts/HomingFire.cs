﻿using UnityEngine;
using System.Collections;

public class HomingFire : MonoBehaviour {

    public GameObject Bullet;
    private Camera _mainCamera;

    [SerializeField]
    float BulletCoordinate; //弾の開始位置

    void Start () {
        float bulletx = getBulletRangeX();

        Instantiate(Bullet, new Vector2(bulletx, BulletCoordinate), transform.rotation);
    }

    float getBulletRangeX()
    {
        return Random.Range(MainController.GetTopLeftLimit().x, MainController.GetBottomRightLimit().x);
    }
}
