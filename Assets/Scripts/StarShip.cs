﻿using UnityEngine;
using System.Collections;

public class StarShip : MonoBehaviour {

    public GameObject bomb;

    private int speed = 2;
    private bool isInvincible = false;
    private int invincibleFrameNum = 0;

    private float? basePosX = null;
    private float? basePosY = null;
    private float dragBasePosX;
    private float dragBasePosY;

    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void Update () {
        MoveWithKeyboard();
        MoveWithTouch();

        //移動範囲限定
        float spriteWidth = GetComponent<SpriteRenderer>().bounds.size.x;
        transform.position =
            (new Vector3(Mathf.Clamp(transform.position.x, getLeftLimit() + spriteWidth/2, getRightLimit() - spriteWidth / 2),
                         Mathf.Clamp(transform.position.y, -1.45f, 0.5f),
                         transform.position.z));

        //無敵時間中の処理
        if (isInvincible)
        {
            if (--invincibleFrameNum <= 0)
            {
                isInvincible = false;
                gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1f);
            }
        }

    }

    void MoveWithKeyboard()
    {
        //キーボード（GamePad）での移動処理
        var vx = Input.GetAxisRaw("Horizontal");
        var vy = Input.GetAxisRaw("Vertical");

        Vector2 direction = new Vector2(vx, vy).normalized;
        GetComponent<Rigidbody2D>().velocity = direction * speed;
    }

    Vector2 GetTapPos()
    {
        if (Input.touchCount > 0)
            return Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
        else
            return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    void MoveWithTouch()
    {
        //タップでの移動処理
        if (Input.touchCount > 0 || Input.GetMouseButton(0))
        {
            Vector2 tapPos = GetTapPos();

            if (basePosX == null && basePosY == null)
            {
                basePosX = transform.position.x;
                basePosY = transform.position.y;
                dragBasePosX = tapPos.x;
                dragBasePosY = tapPos.y;
            }
            else
            {
                GetComponent<Rigidbody2D>().position = new Vector2(
                    (float)basePosX + (tapPos.x - dragBasePosX) * 1.5f,
                    (float)basePosY + (tapPos.y - dragBasePosY) * 1.5f);
            }
        }
        else
        {
            basePosX = null;
            basePosY = null;
        }
    }

    //弾に当たってしまったらさようなら
    void OnTriggerEnter2D()
    {
        if (!isInvincible)
        {
            this.Crash();
        }
    }

    //爆発
    private void Crash()
    {
        //自機を消す
        gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);

        //爆発アニメ
        Instantiate(bomb, this.transform.position, this.transform.rotation);

        //無敵時間付加
        DoInvincible();
    }

    private void DoInvincible()
    {
        invincibleFrameNum = 120;
        isInvincible = true;
    }

    public float getLeftLimit()
    {
        return MainController.GetTopLeftLimit().x;
    }

    public float getRightLimit()
    {
        return MainController.GetBottomRightLimit().x;
    }
}

