﻿using UnityEngine;
using System.Collections;

public class HomingBullet : BulletBase {
    private bool canHoming = true;
    public float HomingDistance;

    // Use this for initialization
    void Start () {
        starShip = GetStarShipObject();
        UpdateBulletVector(starShip.transform);
    }

    // Update is called once per frame
    void Update () {
        if (canHoming)
        {
            UpdateBulletVector(starShip.transform);
        }

        if (Vector2.Distance(starShip.transform.position, transform.position)<HomingDistance)
        {
            canHoming = false;
        }

        Vector2 direction = new Vector2(vx, vy).normalized;
        GetComponent<Rigidbody2D>().velocity = direction * speed;
    }
}
