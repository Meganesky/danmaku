﻿using UnityEngine;
using System.Collections;

//回転する弾
public class RadialBullet : BulletBase {

    public float Degree;
    public float Accel;
    public float RotateSpeed = -8;

    public void Start()
    {
        this.UpdateBulletVector();
    }
    void Update()
    {
        Vector2 direction = new Vector2(vx, vy).normalized;
        GetComponent<Rigidbody2D>().velocity = direction * speed;
        transform.Rotate(0, 0, RotateSpeed);
        speed += Accel;
    }

    //弾の方向を変える
    public void UpdateBulletVector()
    {
        vx = Mathf.Cos(Degree * Mathf.Deg2Rad);
        vy = Mathf.Sin(Degree * Mathf.Deg2Rad);
        transform.rotation = Quaternion.Euler(0, 0, Degree);
    }
}
