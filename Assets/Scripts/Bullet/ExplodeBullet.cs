﻿using UnityEngine;
using System.Collections;

public class ExplodeBullet : BulletBase
{
    public float degree;

    void Start()
    {
        this.UpdateBulletVector();
    }

    public new void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    //弾の方向を変える
    public void UpdateBulletVector()
    {
        vx = Mathf.Cos(degree * Mathf.Deg2Rad);
        vy = Mathf.Sin(degree * Mathf.Deg2Rad);
        transform.rotation = Quaternion.Euler(0,0, degree);
    }
}
