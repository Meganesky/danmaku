﻿using UnityEngine;
using System.Collections;

public class NormalBullet : BulletBase
{
    public new void UpdateBulletVector(Transform target)
    {
        var rad = Mathf.Atan2(
            target.transform.position.y - this.transform.position.y,
            target.transform.position.x - this.transform.position.x);

        vx = Mathf.Cos(rad);
        vy = Mathf.Sin(rad);
        this.speed = Random.value * 2 + 2;
    }
}
