﻿using UnityEngine;
using System.Collections;

public class BulletBase : MonoBehaviour {

    protected float vx;
    protected float vy;
    protected GameObject starShip;

    public float speed = 2;

    void Start()
    {
        starShip = GetStarShipObject();
        UpdateBulletVector(starShip.transform);
    }

    void Update()
    {
        Vector2 direction = new Vector2(vx, vy).normalized;
        GetComponent<Rigidbody2D>().velocity = direction * speed;
    }

    protected GameObject GetStarShipObject()
    {
        return GameObject.FindGameObjectWithTag("Player");
    }

    //カメラ外に出たときの処理
    protected void OnBecameInvisible()
    {
        gameObject.SetActive(false);
        Destroy(gameObject);
    }

    //弾の方向を変える(基本は自機方向に)
    protected virtual void UpdateBulletVector(Transform target)
    {
        var rad = Mathf.Atan2(
            target.transform.position.y - this.transform.position.y,
            target.transform.position.x - this.transform.position.x);

        vx = Mathf.Cos(rad);
        vy = Mathf.Sin(rad);
    }
}
