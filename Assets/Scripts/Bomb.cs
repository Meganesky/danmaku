﻿using UnityEngine;
using UnityEngine.UI;

public class Bomb : MonoBehaviour
{

    void Start()
    {
        //デスカウント＋１
        var deathCount = GameObject.Find("DeathCount").GetComponent<Text>();
        int count = int.Parse(deathCount.text);
        count++;
        deathCount.text = count.ToString().PadLeft(2, '0');
    }

    void OnAnimationEnd()
    {
        Destroy(gameObject);
        resetStarShip();
    }

    void resetStarShip()
    {
        GameObject starship = GameObject.FindGameObjectWithTag("Player");
        starship.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);
        starship.GetComponent<Transform>().position = new Vector2(transform.position.x, transform.position.y);
    }
}