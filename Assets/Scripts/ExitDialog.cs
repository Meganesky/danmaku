﻿using UnityEngine;
using System.Collections;

public class ExitDialog : MonoBehaviour {

    public void OnClickYesBtn()
    {
        Application.Quit();
    }

    public void OnClickNoBtn()
    {
        gameObject.SetActive(false);
    }

}
