﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameOver : MonoBehaviour {

    private bool canRestart = false;

    void RestartKeyScan()
    {
        canRestart = true;
    }

	// Use this for initialization
	void Start () {
        Invoke("RestartKeyScan", 2f);
    }

    // Update is called once per frame
    void Update () {
        if (Input.anyKey && canRestart)
        {
            SceneManager.LoadScene("Scenes/Title");
        }
    }
}
