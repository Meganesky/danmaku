﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Title : MonoBehaviour {
    [SerializeField]
    private GameObject ExitDialog;

    // Use this for initialization
    void Start () {
        GameObject.Find("BackGround").GetComponent<BackGround>().Speed = -0.1f;
    }

    // Update is called once per frame
    void Update () {
        if (Input.anyKeyDown)
        {
            GameStart();
        }
    }

    public void GameStart()
    {
        SceneManager.LoadScene("Scenes/Main");
    }

    public void OnClickExitBtn()
    {
        ExitDialog.SetActive(true);
    }
}
