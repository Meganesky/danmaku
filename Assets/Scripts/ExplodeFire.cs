﻿using UnityEngine;
using System.Collections;

public class ExplodeFire : MonoBehaviour {

    private Camera _mainCamera;

    public int WayBulletNum = 20;
    public GameObject Bullet;

    [SerializeField]
    float BulletCoordinate; //弾の開始位置

    // Use this for initialization
    void Start () {

        float bulletx = getBulletRangeX();

        for (var i = 0; i <= WayBulletNum; i++)
        {
            GameObject obj = (GameObject)Instantiate(Bullet, new Vector2(bulletx, BulletCoordinate), transform.rotation);
            obj.GetComponent<ExplodeBullet>().degree  = 360 / WayBulletNum * i;
        }
    }
    float getBulletRangeX()
    {
        return Random.Range(MainController.GetTopLeftLimit().x, MainController.GetBottomRightLimit().x);
    }

}
