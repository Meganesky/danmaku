﻿using UnityEngine;
using System.Collections;

public class NormalFire : MonoBehaviour {

    private Camera _mainCamera;
    public GameObject Bullet;

    [SerializeField]
    float BulletCoordinate; //弾の開始位置

    // Use this for initialization
    void Start () {
        float bulletx = getBulletRangeX();

        Instantiate(Bullet, new Vector2(bulletx, BulletCoordinate), transform.rotation);
    }

    float getBulletRangeX()
    {
        return Random.Range(MainController.GetTopLeftLimit().x, MainController.GetBottomRightLimit().x);
    }

}
