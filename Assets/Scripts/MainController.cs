﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class MainController : MonoBehaviour
{
    [SerializeField]
    private GameObject ExitDialog;

    [SerializeField]
    private float GameTime;  //ゲーム時間
    [SerializeField]
    private float NoticeTime;    //時報開始

    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject homing;
    [SerializeField] private GameObject explode;
    [SerializeField] private GameObject radial;

    private float StartTime;
    private AudioSource[] audiosources;
    private GameObject restTime;    //残り時間表示オブジェクト

    private string bulletJson = "";

    // Use this for initialization
    void Start()
    {
        //時報の音はあらかじめ持っておく
        audiosources = GetComponents<AudioSource>();

        //時間表示オブジェクトも持っておく
        restTime = GameObject.Find("RestTime");

        //残り時間の計算
        StartTime = Time.time;
        StartCoroutine("TimeCount");

        //時間が来たら弾を出す
        StartCoroutine("NormalBullet", 0f);
        StartCoroutine(HomingBullet(5f, 2));
        StartCoroutine("ExprodeBullet", 10f);
        StartCoroutine("RadialBullet", 30f);
        StartCoroutine( HomingBullet(40f,2) );
        Invoke("StopGame", GameTime);
    }

    // Update is called once per frame
    void Update()
    {
        restTime.GetComponent<Text>().text = GetGameTime();
    }

    IEnumerator TimeCount()
    {
        //カウント時報はちょい早め
        yield return new WaitForSeconds(GameTime - NoticeTime - 1.1f);
        for (;;)
        {
            yield return new WaitForSeconds(1);
            audiosources[1].PlayOneShot(audiosources[1].clip);
        }
    }

    string GetGameTime()
    {
        int restTime = (int)Mathf.Ceil(GameTime - (Time.time - StartTime));
        if (restTime > 0)
            return restTime.ToString().PadLeft(2,'0');
        return "00";
    }

    public void onClickExitBtn()
    {
        ExitDialog.SetActive(true);
    }

    public void StopGame()
    {
        //弾出現停止
        StopAllCoroutines();

        //タイムアップ文字表示
        Instantiate(Resources.Load("Prefabs/GameOver"));

        //背景停止
        GameObject.FindGameObjectWithTag("BG").GetComponent<MonoBehaviour>().enabled = false;

        //弾消し
        foreach (GameObject bulletObj in GameObject.FindGameObjectsWithTag("Bullets"))
        {
            bulletObj.SetActive(false);
        }

        //自機消去
        GameObject.FindGameObjectWithTag("Player").SetActive(false);

        //時報ラスト
        audiosources[0].PlayOneShot(audiosources[0].clip);

    }

    IEnumerator NormalBullet(float starttime)
    {

        yield return new WaitForSeconds(starttime);
        for (;;)
        {
            Instantiate(bullet);
            yield return new WaitForSeconds(0.05f + UnityEngine.Random.Range(0,0.2f) );
        }
    }

    IEnumerator HomingBullet(float starttime, int bulletnum = 4)
    {
        yield return new WaitForSeconds(starttime);
        for (;;)
        {
            for (var i = 0; i < bulletnum; i++)
            {
                Instantiate(homing);
                yield return new WaitForSeconds(0.2f);  //0.2秒待つ
            }
            yield return new WaitForSeconds(3.0f);  //3.0秒に一回発射
        }
    }

    IEnumerator ExprodeBullet(float starttime)
    {

        yield return new WaitForSeconds(starttime);
        for (;;)
        {
            Instantiate(explode);
            yield return new WaitForSeconds(1f);  //1秒につき一発
        }
    }

    IEnumerator RadialBullet(float starttime)
    {

        yield return new WaitForSeconds(starttime);
        for (;;)
        {
            Instantiate(radial);
            yield return new WaitForSeconds(2f);  //2秒で再発射
        }
    }
    public static Vector2 GetTopLeftLimit()
    {
        Camera _mainCamera = getMainCamera();
        Vector2 topLeft = _mainCamera.ScreenToWorldPoint(Vector2.zero);
        return topLeft;
    }
    public static Vector2 GetBottomRightLimit()
    {
        Camera _mainCamera = getMainCamera();
        Vector2 bottomRight = _mainCamera.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        return bottomRight;
    }

    static Camera getMainCamera()
    {
        GameObject obj = GameObject.FindGameObjectWithTag("MainCamera");
        return obj.GetComponent<Camera>();
    }

}