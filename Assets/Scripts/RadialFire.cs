﻿using UnityEngine;
using System.Collections;

//放射状弾発射スクリプト
public class RadialFire : MonoBehaviour
{
    private Camera _mainCamera;

    public GameObject Bullet;
    public int StartDeg;
    public int EndDeg;
    public int SeparateNum;
    public float SpreadTime;

    [SerializeField]
    float BulletCoordinate; //弾の開始位置

    IEnumerator RoundBulletStarter()
    {
        float bulletx = getBulletRangeX();
        AudioSource bulletSe = GetComponent<AudioSource>();

        bool bulletWay = Random.Range(0f, 1f)<0.5;
        for (var i = 0; i < SeparateNum; i++)
        {
            yield return new WaitForSeconds(SpreadTime);  //指定秒待つ
            GameObject obj = (GameObject)Instantiate(Bullet, new Vector2(bulletx, BulletCoordinate), transform.rotation);
            if (bulletWay)
            {
                obj.GetComponent<RadialBullet>().Degree = EndDeg + (StartDeg - EndDeg) / SeparateNum * i;
            }
            else
            {
                obj.GetComponent<RadialBullet>().Degree = StartDeg + (EndDeg - StartDeg) / SeparateNum * i;
            }
            bulletSe.PlayOneShot(bulletSe.clip);
        }
        yield return null;
    }

    // Use this for initialization
    void Start()
    {
        StartCoroutine("RoundBulletStarter");
    }

    float getBulletRangeX()
    {
        return Random.Range(MainController.GetTopLeftLimit().x, MainController.GetBottomRightLimit().x);
    }
}
