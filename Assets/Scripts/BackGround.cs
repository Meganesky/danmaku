﻿using UnityEngine;
using System.Collections;

public class BackGround : MonoBehaviour {

    // スクロールするスピード
    public float Speed;
    private float scrollStartTime;

    void Start()
    {
        scrollStartTime = Time.time;
    }

    // Update is called once per frame
    void Update () {
        if(Speed>0)
            Speed += 0.0001f;
        
        // 時間によってYの値が0から1に変化していく。1になったら0に戻り、繰り返す。
        float y = Mathf.Repeat((Time.time - scrollStartTime) * Speed, 1);

        // Yの値がずれていくオフセットを作成
        Vector2 offset = new Vector2(0, y);

        // マテリアルにオフセットを設定する
        GetComponent<Renderer>().sharedMaterial.SetTextureOffset("_MainTex", offset);
    }
}
